/**
 * Created by Gosia on 14/10/2015.
 */
public class Product {
    private double price;
    private String name;

    public Product(double price, String name, boolean discount, boolean special) {
        this.price = price;
        this.name = name;
    }

    public Product(String productName, double productPrice) {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
