import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gosia on 14/10/2015.
 */
public class Basket {
    List<BasketPosition> basketPositionList = new ArrayList<>();
    private String Client;
    private double Price = 0;
    List<Discount> basketDiscountList = new ArrayList<>();

    public String getClient() {
        return Client;
    }

    public void setClient(String client) {
        Client = client;
    }

    public double getPrice() {
        setPrice();
        return Price;
    }

    public double setPrice() {
        for(BasketPosition ciasteczko : basketPositionList){
         Price = ciasteczko.setTotalprice() + Price;
        }
        return Price;
    }
}
interface  Discount{
    double discount(Basket basket);
    double CheckDiscount(Basket basket);
}
