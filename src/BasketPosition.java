import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gosia on 14/10/2015.
 */
public class BasketPosition {
    private double quantity;
    private double totalprice;
    private Product ciastko;

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getTotalprice() {
        setTotalprice();
        return totalprice;
    }

    public double setTotalprice() {
         return   totalprice = ciastko.getPrice() * getQuantity();
    }
}
